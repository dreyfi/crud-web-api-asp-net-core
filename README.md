# CRUD Web Api built with Asp Net Core 1.1.2 #

This application uses Entity Framework with InMemory Database.

### [GET] http://localhost:52070/api/todo ###

That results in:

200 

[
    {
        "id": 1,
        "name": "Item1",
        "isComplete": false
    }
]

### [GET] http://localhost:52070/api/todo/1 ###

That results in:

200

{
    "id": 1,
    "name": "Item1",
    "isComplete": false
}

### [POST] http://localhost:52070/api/todo ###

Post fields: 

{
	"name":"Walk dog",
	"isComplete":true
}

That results in:

201

{
    "id": 3,
    "name": "Walk dog",
    "isComplete": true
}

### [PUT] http://localhost:52070/api/todo/2 ###

Post fields:

{
	"id": 2,
	"name":"Walk dog 2",
	"isComplete":true
}

That results in:

204 - No content

### [DELETE] http://localhost:52070/api/todo/2 ###

That results in:

204 - No content